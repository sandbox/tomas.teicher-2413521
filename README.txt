--REQUIREMENTS --
PHP Mcrypt extension must be installed and enabled, before enabling this module
See http://php.net/manual/en/book.mcrypt.php for more information, how to install and enable Mcrypt extension 

-- INSTALLATION --

* Install as usual Drupal module, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --
This payment method can be configured by users with permission "Administer payment methods".
1. This payment method can be configured as other payment methods. After enabling the module, Tatrapay should appear in the list of payment methods,
(it is page /admin/commerce/config/payment-methods)
2. After clicking on "TatraPay" in the list, 'Editing reaction rule "TatraPay"'  page appears
(page admin/commerce/config/payment-methods/manage/commerce_payment_tatrapay)
Tatrapay settings (like Merchant ID, safe key) can be edited in "Actions" table


-- HOW IT WORKS

When TatraPay payment method is enabled, it can be chosen by users as payment method during checkout process.
When customer choose TatraPay in checkout process, user is redirected offsite to third-party (TatraPay) website, where the payment will be processed. 
After payment is processed, user is redirected from TatraPay website back to eshop to continue (and finish) checkout process.
After return to eshop, this module automatically update status of payment transaction. There are three various scenarios, how the payment was processed:
Payment was processed succesfully - payment transaction status is set to "success"
Payment failed - payment transaction status is set to "failure"
Information about the payment is missing -  payment transaction status is set to "pending"

Admistrators of eshop can check payment transaction status of particular orders on "Payment" tab of order page (admin/commerce/orders/[ORDER ID]/payment)




